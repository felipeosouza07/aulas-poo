/*Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() 
para criar um novo array contendo apenas os títulos dos livros.*/
namespace livros{
    let livros: any[] = [
        {titulo: "Senhor dos aneis", autor: "Autor 3"},
        {titulo: "Harry Potter", autor: "Autor 3"},
        {titulo: "Mulan", autor: "Disney"},
        {titulo: "O pequeno principe", autor: "Antoine de Saint-Exupéry"}
    ]
    
    let titulos = livros.map((livro) => {
        return livro.titulo
    })
    let autores = livros.map((livro) => {
        return livro.autor
    })

    console.log(titulos);
    console.log(autores);
    /*Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que utilize a função filter() 
    para encontrar todos os livros do autor com valor "Autor 3".Em seguida, utilize a função map() para mostrar apenas os títulos dos
    livros encontrados. O resultado deve ser exibido no console.*/

    let livrosAutor3 = livros.filter((livro) =>{
        return livro.autor === "Autor 3"
    })      
    console.log(livrosAutor3);
    let titulosAutor3 = livrosAutor3.map((livro) => {
        return livro.titulo;
    })
    console.log(titulosAutor3);
    
}
