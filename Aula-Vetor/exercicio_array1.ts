//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.
namespace exercicio_array1{
    let numero: number[] = [1, 2, 3, 4, 5];
    let somaNumeros: number = 0;

    for(let i = 0; i < numero.length; i++){
        somaNumeros = somaNumeros + numero[i]
    }
    console.log(`${somaNumeros}`);

    //Criando uma iteração com multiplicação
    let multi: number = 1;
    for(let i = 0; i < numero.length; i++){
        multi *= numero[i];  
    }
    console.log(`O resultado da multiplicação é: ${multi}`);
    
}