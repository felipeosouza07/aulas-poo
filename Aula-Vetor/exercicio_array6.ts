//Crie um vetor chamado "Alunos contendo três objetos, cada um representando um aluno com as seguintes ppriedades: "nome" (string), "idade" (number),
//e "notas" (array de números).Preencha o vetor com informações fictícias.
//Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente
//com o nome e a idade do aluno.


interface Aluno {
        nome: string;
        idade: number;
        notas: number[];
}
namespace exercicio_array6{

    const alunos: Aluno[] = [
        {nome: "Roger", idade: 20, notas:[10,7,8]},
        {nome: "Adolfo", idade: 23, notas:[6,5,10]},
        {nome: "Romarinho", idade: 49, notas:[10,9,1]},
        {nome: "Cleiton", idade: 18, notas:[3,9,2]},
        {nome: "Lindosvaldo", idade: 33, notas:[1,10,10]},
    ]
    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce((total, nota) => {return total + nota}
        ) / aluno.notas.length
        if (media >= 7){
            console.log(`A média do aluno ${aluno.nome} é igual ${media} e está aprovado`);
        }else {
            console.log(`A média do aluno ${aluno.nome} é igual ${media} e está reprovado`)
        }
        console.log("------------------------------");
        //console.log(aluno.nome,media);
        //console.log("------------------------------");
    })
}