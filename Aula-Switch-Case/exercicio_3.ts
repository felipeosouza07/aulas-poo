namespace exercicio_3{

    let sorvete: string;
    sorvete = "chocolate";

    switch(sorvete){
        case "chocolate": console.log("O sorvete favorito desse usuario é de chocolate");
                        break;
        case "flocos": console.log("O sorvete favorito desse usuario é de flocos");
                        break;
        case "napolitano": console.log("O sorvete favorito desse usuario é de napolitano");
                        break;
        case "morango": console.log("O sorvete favorito desse usuario é de morango");
                        break;
        case "creme": console.log("O sorvete favorito desse usuario é de creme");
                        break;
        case "nutella": console.log("O sorvete favorito desse usuario é de nutella");
                        break;
        case "pistache": console.log("O sorvete favorito desse usuario é de pistache");
                        break;
        case "abacaxi": console.log("O sorvete favorito desse usuario é de abacaxi");
                        break;
        case "maracujá": console.log("O sorvete favorito desse usuario é de maracujá");
                        break;
        default: console.log("Sabor de sorvete não identificado!");
    }

}
